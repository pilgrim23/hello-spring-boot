package com.galvanize.keaganluttrell.hellospringboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ComputeController.class)
public class ComputeControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void compute_NumAndOperatorParams_returnsComputedValueForAddition() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?numbers=1,2,3&operator=+"))
                .andExpect(status().isOk())
                .andExpect(content().string("6"));
    }

    @Test
    void compute_OnlyNumParams_returnsComputedValueForNumbers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?numbers=3,3,3"))
                .andExpect(status().isOk())
                .andExpect(content().string("9"));
    }

    @Test
    void compute_NumAndOperatorParams_returnsComputedValueForSubtraction() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?numbers=10,4,1&operator=-"))
                .andExpect(status().isOk())
                .andExpect(content().string("5"));
    }

    @Test
    void compute_NumAndOperatorParams_returnsComputedValueForMultiplication() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?numbers=2,3,4&operator=*"))
                .andExpect(status().isOk())
                .andExpect(content().string("24"));
    }

    @Test
    void compute_NumAndOperatorParams_returnsComputedValueForDivision() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?numbers=27,3,3&operator=/"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }

    @Test
    void compute_OnlyOperatorParams_returnsZeroForOmittedNumbersParam() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?operator=+"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?operator=-"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?operator=*"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?operator=/"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }

    @Test
    void compute_NumAndOperatorParams_returnsComputedValueWithParamsInAnyOrder() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?operator=-&numbers=10,3,3"))
                .andExpect(status().isOk())
                .andExpect(content().string("4"));
    }

}
