package com.galvanize.keaganluttrell.hellospringboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(VowelCounter.class)
public class VowelCounterTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void vowelCounter_stringParam_returnsNumberOfVowels() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/countVowels?word=alphabet"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }

    @Test
    void vowelCounter_stringParam_returnsNumberOfVowelsCaseInsensitive() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/countVowels?word=AaEeIiOoUu"))
                .andExpect(status().isOk())
                .andExpect(content().string("10"));
    }

    @Test
    void vowelCounter_NoParams_returnsNumberOfVowels() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/countVowels"))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }
}
