package com.galvanize.keaganluttrell.hellospringboot;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(SearchAndReplace.class)
public class SearchAndReplaceTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void replacer_SearchAndReplaceParams_ReturnsReplacedString() throws Exception {
        mockMvc.perform(post("/phrase?search=cat&replace=fox")
                .contentType(MediaType.TEXT_PLAIN)
                .content("The cat jumped over the dog"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("The fox jumped over the dog"));
    }

    @Test
    void replacer_SearchAndReplaceParams_ReturnsStringNoFoundWord() throws Exception {
        mockMvc.perform(post("/phrase?search=hippo&replace=fox")
                .contentType(MediaType.TEXT_PLAIN)
                .content("The cat jumped over the dog"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("The cat jumped over the dog"));
    }
}
