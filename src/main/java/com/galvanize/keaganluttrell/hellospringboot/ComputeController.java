package com.galvanize.keaganluttrell.hellospringboot;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ComputeController {

    @GetMapping("/compute")
    public String computeParams(
            @RequestParam(defaultValue = "0") String numbers,
            @RequestParam(defaultValue = "+") String operator
    ) {
        String[] nums = numbers.split(",");
        int result = Integer.parseInt(nums[0]);
        for (int i = 1; i < nums.length; i++) {
            switch (operator) {
                case "+":
                    result += Integer.parseInt(nums[i]);
                    break;
                case "-":
                    result -= Integer.parseInt(nums[i]);
                    break;
                case "*":
                    result *= Integer.parseInt(nums[i]);
                    break;
                case "/":
                    result /= Integer.parseInt(nums[i]);
                    break;
            }
        }

        return String.valueOf(result);
    }
}
