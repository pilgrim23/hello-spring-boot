package com.galvanize.keaganluttrell.hellospringboot;

import org.springframework.web.bind.annotation.*;

@RestController
public class SearchAndReplace {

    @GetMapping("/phrase")
    public String getPhrase() {
        return "The cat jumped over the dog";
    }

    @PostMapping("/phrase")
    public String replacer(
            @RequestBody String body,
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "") String replace
    ) {
        String[] words = body.split(" ");
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(search)) {
                words[i] = replace;
            }
        }
        return String.join(" ", words);
    }
}
